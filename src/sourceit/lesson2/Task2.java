package sourceit.lesson2;

import java.util.Scanner;

/**
 * Created by Надиа on 06.03.2017.
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Введите число 1: ");
        int a = in.nextInt();
        System.out.println("Введите число 2: ");
        int b = in.nextInt();
        System.out.println("Введите число 3: ");
        int c = in.nextInt();  // ax^2 + bx + c = 0.

        double x1, x2;
        double D = (Math.pow(a, 2)) - 4 * a * c;

        if (D > 0) {
            x1 = (-b + Math.sqrt(D)) / (2 * a);
            x2 = (-b - Math.sqrt(D)) / (2 * a);
            System.out.println("Ответ: " + x1 + " " + x2);
        } else if (D == 0) {
            x1 = x2 = -(b / (2 * a));
            System.out.println("Ответ: " + x1 + " " + x2);
        } else if (D < 0) {
            System.out.println("У уравнения действительных корней нет.");
        }


    }
}
