package sourceit.lesson2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Comparator;

/**
 * Created by Надиа on 06.03.2017.
 */
public class Task5 {
    public static void main(String[] args) {

        class StringLengthSort implements Comparator<String> {

            public int compare(String o1, String o2) {
                if (o1.length() > o2.length()) {
                    return 1;
                } else {
                    if (o1.length() < o2.length()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
        }
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();

        String[] arrayOfRows = new String[a];
        int[] arrayOfRowsLength = new int[a];
        Comparator<String> stringLengthComparator = new StringLengthSort();

        for (int i = 0; i < a; i++) {
            System.out.println(String.format("Введите строку %d: ", i + 1));
            String row = in.next();
            arrayOfRows[i] = row;
        }

        Arrays.sort(arrayOfRows, stringLengthComparator);
        for (String str : arrayOfRows) {
            System.out.println(str + " - " + str.length());
        }

    }
}

