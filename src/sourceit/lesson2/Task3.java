package sourceit.lesson2;

import java.util.Scanner;

/**
 * Created by Надиа on 06.03.2017.
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();

        String[] arrayOfRows = new String[a];
        int[] arrayOfRowsLength = new int[a];

        for (int i = 0; i < a; i++) {
            System.out.println(String.format("Введите строку %d: ", i + 1));
            String row = in.next();
            arrayOfRows[i] = row;
            arrayOfRowsLength[i] = row.length();
        }
        int minLength = arrayOfRows.length;
        String minLengthRow = arrayOfRows[0];


        for (int i = 0; i < a; i++) {
            if (arrayOfRowsLength[i] < minLength) {
                minLength = arrayOfRowsLength[i];
                minLengthRow = arrayOfRows[i];

                System.out.println("Самая короткая строка: " + minLengthRow);
                System.out.println("Ее длинна: " + minLength);
            }

        }

    }
}