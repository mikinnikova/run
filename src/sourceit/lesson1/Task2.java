package sourceit.lesson1;

/**
 * Created by Надиа on 25.02.2017.
 */
public class Task2 {
    public static void main(String[] args) {
        int Jupiter = 778_000_000;
        int Saturn = 1_043_000_000;
        int Mars = 228_000_000;
        int Venus = 108_000_000;
        int Earth = 150_000_000;


        int kmJ = Jupiter - Earth;
        int kmS = Saturn - Earth;
        int kmM = Mars - Earth;
        int kmV = Earth - Venus;


        System.out.println("Юпитер:" + " " + Jupiter + "км до солнца");
        System.out.println("Сатурн:" + " " + Saturn + "км до солнца");
        System.out.println("Марс:" + " " + Mars + "км до солнца");
        System.out.println("Венера:" + " " + Venus + "км до солнца");


        System.out.println("Юпитер:" + " " + kmJ + "км до Земли");
        System.out.println("Сатурн:" + " " + kmS + "км до Земли");
        System.out.println("Марс:" + " " + kmM + "км до Земли");
        System.out.println("Венера:" + " " + kmV + "км до Земли");
    }
}


